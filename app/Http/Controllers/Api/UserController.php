<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TrashNet\Users\Commands\GetUserCommand;
use TrashNet\Users\Commands\RegisterUserCommand;
use TrashNet\Users\Commands\UpdateUserCommand;

class UserController extends Controller
{
	/**
	 * @param Request $request
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function get(Request $request)
	{
		return (new GetUserCommand($request))->handle();
	}
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function register(Request $request)
	{
		return (new RegisterUserCommand($request))->handle();
    }
	
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function update(Request $request)
	{
		return (new UpdateUserCommand($request))->handle();
    }
}
