<?php

namespace App\Http\Controllers;

use TrashNet\TrashLikes\Commands\AddLikeToTrashPointCommand;
use TrashNet\TrashLikes\TrashLike;
use Illuminate\Http\Request;

class TrashLikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return (new AddLikeToTrashPointCommand($request))->handle();
    }

    /**
     * Display the specified resource.
     *
     * @param  \TrashNet\TrashLikes\TrashLike  $trashLike
     * @return \Illuminate\Http\Response
     */
    public function show(TrashLike $trashLike)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \TrashNet\TrashLikes\TrashLike  $trashLike
     * @return \Illuminate\Http\Response
     */
    public function edit(TrashLike $trashLike)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TrashNet\TrashLikes\TrashLike  $trashLike
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrashLike $trashLike)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TrashNet\TrashLikes\TrashLike  $trashLike
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrashLike $trashLike)
    {
        //
    }
}
