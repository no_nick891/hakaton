<?php

namespace App\Http\Controllers;

use TrashNet\TrashPoints\Commands\GetTrashPointCommand;
use TrashNet\TrashPoints\Commands\StoreTrashPointCommand;
use TrashNet\TrashPoints\TrashPoint;
use Illuminate\Http\Request;

class TrashPointController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 * @return \Illuminate\Http\Response
	 */
    public function index(Request $request)
    {
	    return (new GetTrashPointCommand($request))->handle();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return (new StoreTrashPointCommand($request))->handle();
    }

    /**
     * Display the specified resource.
     *
     * @param  \TrashNet\TrashPoints\TrashPoint  $trashPoint
     * @return \Illuminate\Http\Response
     */
    public function show(TrashPoint $trashPoint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \TrashNet\TrashPoints\TrashPoint  $trashPoint
     * @return \Illuminate\Http\Response
     */
    public function edit(TrashPoint $trashPoint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \TrashNet\TrashPoints\TrashPoint  $trashPoint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrashPoint $trashPoint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \TrashNet\TrashPoints\TrashPoint  $trashPoint
     * @return \Illuminate\Http\Response
     */
    public function destroy(TrashPoint $trashPoint)
    {
        //
    }
}
