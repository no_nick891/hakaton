<?php  namespace TrashNet;

use Illuminate\Support\Facades\Request;
use TrashNet\Users\UserRepo;

class BaseCommand {

	protected $request;
	
	public $user;
	
	/**
	 * BaseCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
		
		$this->userRepo = new UserRepo();
		
		$this->user = $this->userRepo->getByToken(data_get($request, 'access_token', ''));
	}
}