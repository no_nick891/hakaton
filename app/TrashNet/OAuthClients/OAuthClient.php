<?php namespace TrashNet\OAuthClients;

use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{
	public $table = 'oauth_clients';
	
	public $fillable = ['user_id', 'secret'];
	
	public $attributes = ['name' => '', 'redirect' => '', 'personal_access_client' => 0, 'password_client' => 0, 'revoked' => 0];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function user()
	{
		return $this->hasOne('App\User', 'id');
	}
}
