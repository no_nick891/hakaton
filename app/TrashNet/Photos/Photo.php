<?php

namespace TrashNet\Photos;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $fillable = ['path'];
}
