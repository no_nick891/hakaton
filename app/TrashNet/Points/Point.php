<?php

namespace TrashNet\Points;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    public $fillable = ['latitude', 'longitude'];
}
