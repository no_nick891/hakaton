<?php  namespace TrashNet\TrashLikes\Commands;

use Illuminate\Support\Facades\Request;
use TrashNet\BaseCommand;
use TrashNet\TrashLikes\TrashLike;
use TrashNet\TrashPoints\TrashPoint;

class AddLikeToTrashPointCommand extends BaseCommand {

	protected $request;

	/**
	 * AddLikeToTrashPointCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		parent::__construct($request);
	}
	
	public function handle()
	{
		$trashPointId = (int)data_get($this->request, 'trash_point_id', 0);
		
		$trashLike = new TrashLike();
		
		$trashLike->fill([
			'trash_point_id' => $trashPointId,
		    'user_id' => $this->user->id
		]);
		
		$result = $trashLike->save();
		
		if ($result)
		{
			\DB::table('trash_points')
				->whereId($trashPointId)
				->increment('like_count');
		}
		
		return response(['result' => $result], 200);
	}
}