<?php

namespace TrashNet\TrashLikes;

use Illuminate\Database\Eloquent\Model;

class TrashLike extends Model
{
    public $fillable = ['trash_point_id', 'user_id'];
}
