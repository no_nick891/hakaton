<?php

namespace TrashNet\TrashPointPhotos;

use Illuminate\Database\Eloquent\Model;

class TrashPointPhoto extends Model
{
	public $fillable = ['photo_id', 'trash_point_id'];
	
	public $attributes = ['trash_point_id' => 0];
	
	public function photo()
	{
		return $this->hasOne('TrashNet\Photos\Photo', 'id', 'photo_id');
    }
}
