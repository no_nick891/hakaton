<?php  namespace TrashNet\TrashPoints\Commands;

use Illuminate\Support\Facades\Request;
use TrashNet\BaseCommand;
use TrashNet\TrashPoints\TrashPoint;

class GetTrashPointCommand extends BaseCommand {

	protected $request;

	/**
	 * AddTrashPointCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		parent::__construct($request);
	}
	
	public function handle()
	{
		return response(['result' => (new TrashPoint())
			->select([
				'tp.*',
				'p.latitude',
				'p.longitude',
				'ph.path',
				\DB::raw('GROUP_CONCAT(tt.name SEPARATOR ",") as type'),
			    \DB::raw('GROUP_CONCAT(ttttp.checked SEPARATOR ",") as checked')
			])
			->from('trash_points as tp')
			->leftJoin('points as p', 'tp.point_id', '=', 'p.id')
			->leftJoin('trash_type_to_trash_points as ttttp', 'ttttp.trash_point_id', '=', 'tp.id')
			->leftJoin('trash_types as tt', 'tt.id', '=', 'ttttp.trash_type_id')
			->leftJoin('trash_point_photos as tpp', 'tpp.trash_point_id', '=', 'tp.id')
			->leftJoin('photos as ph', 'ph.id', '=', 'tpp.photo_id')
			->groupBy('tp.id')
			->get()]);
	}
}