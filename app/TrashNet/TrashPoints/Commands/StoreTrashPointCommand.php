<?php namespace TrashNet\TrashPoints\Commands;

use TrashNet\BaseCommand;
use TrashNet\TrashPoints\TrashPoint;
use TrashNet\TrashTypeToTrashPoints\TrashTypeToTrashPoint;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use TrashNet\Photos\Photo;
use TrashNet\Points\Point;
use TrashNet\TrashPointPhotos\TrashPointPhoto;
use TrashNet\TrashTypes\TrashType;

class StoreTrashPointCommand extends BaseCommand
{
	protected $request;
	
	/**
	 * StoreTrashPointCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		parent::__construct($request);
	}
	
	public function handle()
	{
		$force = data_get($this->request, 'force', false);
		
		$force = !($force === false || $force === 'false');
		
		$geoPoint = $this->request->only([
			'latitude',
			'longitude'
		]);
		
		if (!$force)
		{
			$nearPoints = $this->_findNearPoints($geoPoint);
			
			if (count($nearPoints) > 0)
			{
				return response([
					'result' => 'Have points near current.',
					'points' => $nearPoints
				]);
			}
		}
		
		$point = new Point($geoPoint);
		
		$trashPoint = new TrashPoint(array_merge($this->request->only([
			'name', 'description', 'amount'
		]), ['user_id' => $this->user->id]));
		
		$photo = false;
		
		\Log::error(print_r($this->request->all(), true));
		
		if ($this->request->hasFile('image'))
		{
			$photo = $this->_savePhoto($this->request->file('image'), $this->user->id);
		}
		
		$savedPoint = $trashPoint->point()->save($point);
		
		$trashPoint->point_id = $savedPoint->id;
		
		$result = $trashPoint->save();
		
		$types = $this->request->only([
			'metall', 'plastic', 'paper', 'glass',
			'construction_garbage', 'household_waste',
			'tires', 'animal_remains'
		]);
		
		$this->_addTrashTypeToTrashPoint($trashPoint->id, $types);
		
		if ($photo)
		{
			$photoId = data_get($photo, 'id', 0);
			
			$trashPointPhotos = (new TrashPointPhoto())->fill([
				'photo_id' => $photoId,
				'trash_point_id' => $trashPoint->id
			]);
			
			$trashPointPhotos->save();
		}
		
		return response(['result' => (string)$result]);
	}
	
	/**
	 * @param $file
	 * @param $userId
	 * @return Photo
	 */
	private function _savePhoto($file, $userId)
	{
		$extension = $file->getClientOriginalExtension();
		
		$imagePath = $file->store('public/points/' . $userId);
		
		$image = Image::make(Storage::get($imagePath))->encode($extension, 70);
		
		Storage::put($imagePath, $image);
		
		$photo = new Photo(['path' => asset('storage/' . str_replace('public/', '', $imagePath))]);
		
		$photo->save();
		
		return $photo;
	}
	
	/**
	 * @param $pointId
	 * @param $types
	 */
	private function _addTrashTypeToTrashPoint($pointId, $types)
	{
		$typesResult = [];
		
		foreach ($types as $type => $value)
		{
			$typesResult[$type] = $value === 'true' || $value === true;
		}
		
		$trashTypeToPoint = new TrashType();
		
		$typesObj = $trashTypeToPoint
			->whereIn('name', array_keys($types))
			->get();
		
		foreach ($typesObj as $obj)
		{
			$trashTypeToTrashPoint = new TrashTypeToTrashPoint();
			
			$trashTypeObj = $trashTypeToTrashPoint
				->where('trash_point_id', $pointId)
				->where('trash_type_id', $obj->id)
				->first();
			
			$data = [
				'trash_point_id' => $pointId,
				'trash_type_id' => $obj->id,
				'checked' => (int)$typesResult[$obj->name]
			];
			
			if ($trashTypeObj)
			{
				$trashTypeObj->update($data);
			}
			else
			{
				$trashTypeToTrashPoint->fill($data);
				
				$trashTypeToTrashPoint->save();
			}
		}
	}
	
	/**
	 * @param $geoPoint
	 */
	private function _findNearPoints($geoPoint)
	{
		$sql = "select * from
				(
					select
					 	`tp`.*,
						6371 * acos(cos( radians( {$geoPoint['latitude']} ) ) * cos( radians( points.latitude ) ) * cos( radians( {$geoPoint['longitude']} ) - radians( points.longitude ) ) + sin(radians( {$geoPoint['latitude']} ) ) * sin( radians( points.latitude ))) as distance
					from `points`
					left join `trash_points` as `tp` on `tp`.`point_id` = `points`.`id`
				) as temp
				where `distance` < 0.3";
		
		return \DB::select(\DB::raw($sql));
	}
}