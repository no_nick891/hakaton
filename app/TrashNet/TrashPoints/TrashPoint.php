<?php namespace TrashNet\TrashPoints;

use Illuminate\Database\Eloquent\Model;

class TrashPoint extends Model
{
	public $fillable = ['name', 'description', 'user_id'];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function point()
	{
		return $this->hasOne('TrashNet\Points\Point', 'id', 'point_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function photos()
	{
		return $this->hasMany('TrashNet\Points\Point', 'trash_point_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function likes()
	{
		return $this->hasMany('TrashNet\TrashLikes\TrashLike', 'trash_point_id');
    }
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function trashTypes()
	{
		return $this->hasMany('TrashNet\TrashTypeToTrashPoints\TrashTypeToTrashPoint', 'trash_point_id');
    }
}
