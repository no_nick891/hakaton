<?php

namespace TrashNet\TrashTypeToTrashPoints;

use Illuminate\Database\Eloquent\Model;

class TrashTypeToTrashPoint extends Model
{
    public $fillable = ['trash_point_id', 'trash_type_id', 'checked'];
    
    public $timestamps = false;
}
