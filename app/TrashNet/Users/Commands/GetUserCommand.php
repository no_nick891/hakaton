<?php namespace TrashNet\Users\Commands;

use App\User;
use Illuminate\Support\Facades\Hash;
use TrashNet\OAuthClients\OAuthClient;

class GetUserCommand
{
	private $request;
	
	/**
	 * GetUserCommand constructor.
	 * @param $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$target = $this->request->only(['mobile', 'password']);
		
		$mobile = data_get($target, 'mobile', false);
		
		$password = (data_get($target, 'password', ''));
		
		if (!$mobile) return response(['result' => 'Set mobile phone'], 404);
		
		$user = new User();
		
		$finedUser = $user->where('mobile', $mobile)
						->first();
		
		if (Hash::check($password, $finedUser->password))
		{
			$oauth = new OAuthClient();
			
			$secret = $oauth
				->where('user_id', $finedUser->id)
				->first();
			
			if ($secret)
			{
				$secret->fill([
					'user_id' => $finedUser->id,
					'secret' => bcrypt($finedUser->mobile)
				]);
				
				$secret->save();
			}
			else
			{
				$secret = new OAuthClient([
					'user_id' => $finedUser->id,
					'secret' => bcrypt($finedUser->mobile)
				]);
				
				$secret->save();
			}
			
			return response(array_merge($finedUser->toArray(), [
				'result' => true,
			    'access_token' => $secret->secret
			]), 200);
		}
		
		return response(['result' => 'Wrong password'], 200);
	}
}