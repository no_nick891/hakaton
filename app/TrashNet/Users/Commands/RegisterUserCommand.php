<?php namespace TrashNet\Users\Commands;

use App\User;
use Illuminate\Http\Request;
use TrashNet\OAuthClients\OAuthClient;

class RegisterUserCommand
{
	private $request;
	
	/**
	 * RegisterUserCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$user = new User();
		
		$attributes = $this->request->all();
		
		$attributes['password'] = bcrypt($attributes['password']);
		
		$user->fill($attributes);
		
		$result = $user->save();
		
		if ($result)
		{
			$oauth = new OAuthClient([
				'user_id' => $user->id,
				'secret' => bcrypt($user->mobile)
			]);
			
			$oauth->save();
			
			return response([
				'access_token' => $oauth->secret,
				'result' => $result
			], 200);
		}
		else
		{
			return response(['result' => false], 404);
		}
	}
}