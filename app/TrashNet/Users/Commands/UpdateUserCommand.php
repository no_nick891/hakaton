<?php namespace TrashNet\Users\Commands;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use TrashNet\OAuthClients\OAuthClient;

class UpdateUserCommand
{
	private $request;
	
	/**
	 * UpdateUserCommand constructor.
	 * @param Request $request
	 */
	public function __construct($request)
	{
		$this->request = $request;
	}
	
	public function handle()
	{
		$data = $this->request->all();
		
		if ((string)$this->request->code !== '1234')
		{
			return response()->json(['Please enter valid code']);
		}
		
		$user = new User();
		
		if ($temp = $user->where('mobile', $data['mobile'])->first())
		{
			$user = $temp;
		}
		
		if (isset($data['password']))
		{
			$data['password'] = Hash::make($data['password']);
		}
		
		$data['avatar'] = '';
		
		$user->fill($data);
		
		$result = $user->save();
		
		/*if ($this->request->hasFile('avatar'))
		{
			$file = $this->request->file('avatar');
			
			$ext = $file->getClientOriginalExtension();
			
			$img = Image::make($file);
			
			$path = 'profile/' . $user->id . '/avatar.' . $ext;
			
			$file = $img->resize(300, 300)->stream('jpg', 70);
			
			Storage::put($path, $file);
			
			$user->avatar = $path;
			
			$user->save();
		}*/
		
		return response(['result' => $result], 200);
	}
}