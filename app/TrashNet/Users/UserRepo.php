<?php namespace TrashNet\Users;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserRepo
{
	/**
	 * @var Model
	 */
	private $model;
	
	
	/**
	 * UserRepo constructor.
	 */
	public function __construct()
	{
		$this->model = new User();
	}
	
	/**
	 * @param $token
	 * @return mixed
	 */
	public function getByToken($token)
	{
		return $this->model
			->select('users.*')
			->leftJoin('oauth_clients as oc', 'oc.user_id', '=', 'users.id')
			->where('oc.secret', $token)
			->first();
	}
}