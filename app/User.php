<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use QiuTuleng\PhoneVerificationCodeGrant\Interfaces\PhoneVerificationCodeGrantUserInterface;

class User extends Authenticatable implements PhoneVerificationCodeGrantUserInterface
{
	use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'surname',
		'patronymic', 'gender', 'city'
    ];
    
    protected $attributes = [
    	'city' => '', 'avatar' => '', 'name' => '', 'password' => '', 'surname' => '',
	    'patronymic' => '', 'gender' => 0
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	/**
	 * Find or create a user by phone number
	 *
	 * @param $phoneNumber
	 * @return \Illuminate\Database\Eloquent\Model|null
	 */
	public function findOrCreateForPassportVerifyCodeGrant($phoneNumber)
	{
		// If you need to automatically register the user.
		return static::firstOrCreate(['mobile' => $phoneNumber]);
		
		// If the phone number is not exists in users table, will be fail to authenticate.
		// return static::where('mobile', '=', $phoneNumber)->first();
	}
	
	/**
	 * Check the verification code is valid.
	 *
	 * @param $verificationCode
	 * @return boolean
	 */
	public function validateForPassportVerifyCodeGrant($verificationCode)
	{
		// Check verification code is valid.
		// return \App\Code::where('mobile', $this->mobile)->where('code', '=', $verificationCode)->where('expired_at', '>', now()->toDatetimeString())->exists();
		return true;
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function oauthClients()
	{
		return $this->hasOne('TrashNet\OAuthClients\OAuthClient', 'user_id');
	}
}
