<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname');
            $table->string('patronymic');
            $table->tinyInteger('gender')->default(0);
	        $table->string('city');
	        $table->string('avatar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->dropColumn('surname');
	        $table->dropColumn('patronymic');
	        $table->dropColumn('gender');
	        $table->dropColumn('city');
	        $table->dropColumn('avatar');
        });
    }
}
