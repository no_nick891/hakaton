<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeAmountFieldsTrashPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trash_points', function (Blueprint $table) {
            $table->integer('type_id')->after('point_id')->default(0);
            $table->enum('amount', ['Мало', 'Средне', 'Много'])->after('description')->default('Мало');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trash_points', function (Blueprint $table) {
	        $table->dropColumn('type_id');
	        $table->dropColumn('amount');
        });
    }
}
