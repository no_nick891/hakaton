<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckedColumnTrashTypeToTrashPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trash_type_to_trash_points', function (Blueprint $table) {
            $table->tinyInteger('checked')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trash_type_to_trash_points', function (Blueprint $table) {
            $table->dropColumn('checked');
        });
    }
}
