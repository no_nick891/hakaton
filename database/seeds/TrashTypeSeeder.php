<?php

use Illuminate\Database\Seeder;

class TrashTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('trash_type')->insert([
	        ['name' => 'Пластик'],
	        ['name' => 'Металл'],
	        ['name' => 'Бумага'],
	        ['name' => 'Стекло'],
        	['name' => 'Строительный мусор'],
        	['name' => 'Бытовые отходы'],
        	['name' => 'Покрышки'],
        	['name' => 'Останки животных']
        ]);
    }
}
