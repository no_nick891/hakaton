<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('user', 'Api\UserController@get');

Route::post('user', 'Api\UserController@update');

Route::group(['prefix' => 'trash-point'], function() {
	
	Route::get('get', 'TrashPointController@index');
	
	Route::post('store', 'TrashPointController@store');
	
});

Route::group(['prefix' => 'point-like'], function() {
	
	Route::post('store', 'TrashLikeController@store');
	
});